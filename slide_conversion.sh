#!/bin/bash

#SBATCH --mem=2G
#SBATCH --cpus-per-task=4
#SBATCH -t 48:00:00
#SBATCH --job-name=slide_conversion
#SBATCH -e ~/logs/slide_conversion.err.txt
#SBATCH -o ~/logs/slide_conversion.out.txt


if [ -z "$1" ]
  then
      echo "Input directory required."
      exit 1
fi

if [ ! -d $1 ]; then
    echo "Directory $1 does not exist."
    exit 1
fi
if [ -z "$2" ]
  then
      echo "Output directory required."
      exit 1
fi

if [ ! -d $1 ]; then
    echo "Directory $2 does not exist."
    exit 1
fi


module load Nextflow/22.04.0
module load X11
module load Perl
module load Java

nextflow run /g/ellenberg/JKH/scripts/slides-conversion --inputdir $1 --outputdir $2 --series 2 --compression zlib
