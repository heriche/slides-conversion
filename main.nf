#!/usr/bin/env nextflow
nextflow.enable.dsl=2

/*  
   This workflow processes a directory of pyramidal images in 
   PerkinElmer qptiff format. 
   For each image file, it first exports one series of the pyramid 
   in ome-tiff format then rewrites the ome-xml header to rename
   the channels using biomarker names found in the xml annotations.
   Optionally if details of an S3 bucket are provided in a config file, 
   the images will be copied to the bucket.

   Usage: 
      nextflow run convert_slides --inputdir </path/to/input_dir> \
      --outputdir </path/to/output_dir> --series <pyramid series index> \
      --compression <LZW|JPEG-2000|JPEG-2000 Lossy|JPEG|zlib>

*/

/* Get all .tif files in inputdir subdirectories.
   The workflow currently assumes a depth of one level only.

   Note that baseName strips the file name extension after the last dot.*/
inputFile = Channel
    .fromPath( "${params.inputdir}/**/*.tif" ) 
    .map { file -> def sample_id = file.getParent().getName()
	  return tuple(file.baseName, sample_id, file) }
//    .groupTuple(sort: true) // use if depth > 1, not tested yet


// We can't use nextflow's built-in S3 access mechanisms because EMBL's 
// minio setup currently doesn't support virtual host-style access. 
process setup_mc {
    output:
    stdout emit: verbose
    
    // This will silently overwwrite the alias if it already exists
    script:
    """
    mc alias set ${MC_ALIAS} ${AWS_S3_ENDPOINT} ${AWS_ACCESS_KEY_ID} ${AWS_SECRET_ACCESS_KEY}
    """
}

process convert_to_ometiff {
 
    input:
    tuple val(fileName), val(sample_id), file(imgFile)
    output:
    tuple val(fileName), val(sample_id), file("${fileName}.tmp.ome.tiff"), emit: tmpFile
    
    script:
    """
    bfconvert -series ${params.series} -compression ${params.compression} ${imgFile} ${fileName}.tmp.ome.tiff
    """
}

process rename_channels {

    publishDir "${params.outputdir}/${sample_id}", mode: 'copy', overwrite: true
    input:
    tuple val(fileName), val(sample_id), file(tmpTiff)
    output:
    tuple val(fileName), val(sample_id), file("${fileName}.ome.tiff"), emit: convertedFile 
    stdout emit: verbose
    
    script:
    """
    rename_channels_to_biomarkers_in_ome_xml.pl -v -i ${tmpTiff} -o ${fileName}.ome.tiff
    """
}

process push_to_s3 {
    
    input:
    tuple val(fileName), val(sample_id), file(omeTiff)
    output:
    val("${sample_id}/${fileName}.ome.tiff"), emit: s3Object
    stdout emit: verbose

    script:
    """
    mc cp ${omeTiff} "${MC_ALIAS}/${BUCKET_NAME}/${sample_id}/${fileName}.ome.tiff"
    """
}

process update_image_list {

    publishDir "${params.outputdir}", mode: 'copy', overwrite: true
    input:
    val fileName
    output:
    file 'image_table.csv'
    stdout emit: verbose

    script:
    """
    {
      mc cat "${MC_ALIAS}/${BUCKET_NAME}/image_table.csv" ${fileName} > 'tmp_table.csv'
      awk 'NR==1 || !visited[\$0]++' tmp_table.csv > image_table.csv # Remove duplcated lines
    } || { # file image_table.csv not in bucket
      echo 'imageName' > 'tmp_table.csv' # add header
      cat tmp_table.csv ${fileName} > image_table.csv 
    }
    mc cp 'image_table.csv' "${MC_ALIAS}/${BUCKET_NAME}/image_table.csv"
    """
}

workflow {
    if (binding.hasVariable('BUCKET_NAME')) { // check if there is a variable named BUCKET_NAME
	setup_mc()
    }
    convert_to_ometiff(inputFile)
    rename_channels(convert_to_ometiff.out.tmpFile)
    if (binding.hasVariable('BUCKET_NAME')) {
	push_to_s3(rename_channels.out.convertedFile)
	update_image_list(push_to_s3.out.s3Object.collectFile(name: 'new_images.txt', newLine: true).view())
    }
}
