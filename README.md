## Whole slide image conversion

This workflow is meant to process a directory of pyramidal images in PerkinElmer qptiff format. For each image file, it first exports one series of the pyramid in ome-tiff format then rewrites the ome-xml header to rename the channels using biomarker names included in the xml annotations.
If details of an S3 bucket are provided in a config file, the converted images are copied to the bucket.
	
 #### Usage:
 ```
nextflow run /path/to/slides-conversion --inputdir </path/to/input_dir> \
--outputdir </path/to/output_dir> \
--series <pyramid series index> \
--compression <LZW|JPEG-2000|JPEG-2000 Lossy|JPEG|zlib>
```
	  
To run directly from the repository, add the following to $HOME/.nextflow/scm:

```
providers {
  embl {
    server = "https://git.embl.de"
    platform = "gitlab"
  }	
}
```
then 

```
nextflow run https://git.embl.de/heriche/slides-conversion -hub embl --inputdir </path/to/input_dir> \
--outputdir </path/to/output_dir> --series <pyramid series index> \
--compression <LZW|JPEG-2000|JPEG-2000 Lossy|JPEG|zlib>
```

To copy the files to an EMBL S3 bucket, add the following to nexflow.config:

```
env {
    AWS_S3_ENDPOINT = "https://s3.embl.de"
    AWS_ACCESS_KEY_ID = "<accessID>"
    AWS_SECRET_ACCESS_KEY = "<accessKey>"
    BUCKET_NAME = "<bucketName>"
    MC_ALIAS = "<some_alias>"
}
```

## Dependencies

The workflow uses bfconvert and tiffcomment from the bftools suite, the minio client mc and a custom perl script. These are provided in the bin directory.
The perl script is used to edit the ome-xml data and requires the module XML::Twig. To install this module:

```
PERL_MM_USE_DEFAULT=1 perl -MCPAN -e "install XML::Twig"
```

## Use case

We need to put whole slide images in a private S3 bucket from which they can be accessed using a pre-signed URL by a viv-based image viewer (i.e. avivator, vitessce).
This requires the images to be in ome-tiff format of smaller size (hence extracting and compressing one resolution level of the image pyramid) and with channels named meaningfully (hence the channel renaming step of the workflow).

## To run on the EMBL cluster

Note: the nextflow slurm executor didn't work so we're running the workflow as a whole as a slurm job.

1- Log into a cluster login node

2- Install perl module XML::Twig

```
module load Perl
PERL_MM_USE_DEFAULT=1 perl -MCPAN -e "install XML::Twig"
```

3- Clone the workflow repository

```
git clone https://git.embl.de/heriche/slides-conversion
```

4- Create file ~/.nextflow/nextflow.config with the relevant values for S3 bucket access (use https://git.embl.de/heriche/slides-conversion/nexflow.config as template)

5- Create a logs directory

```
mkdir ~/logs
```

6- Submit job (provide the correct paths for input and output directories)

```
sbatch slides-conversion/slides-conversion.sh /scratch/<username>/<original> /scratch/<username>/<converted>
```

7- Follow workflow progression

```
tail -f ~/logs/slide_conversion.out.txt
```
