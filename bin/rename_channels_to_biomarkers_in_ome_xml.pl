#!/usr/bin/env perl

use warnings;
use strict;
use File::Basename;
use Cwd;
use File::Copy;
use XML::Twig;
use Getopt::Long;


my ($help, $v, $infile, $outfile);
GetOptions ( 'help'=>\$help,
	     'verbose' => \$v,
	     'input=s' => \$infile,
	     'output=s' => \$outfile
           );
usage() if ($help || !$infile);
if (!$infile) {
  die "\nERROR: Need an ome.tiff file.\n";
}
unless (-f $infile) {
  die "File $infile not found.\n";
}
my ($filename, $dirs, $suffix) = fileparse($infile, qr/\.ome\.tif*/);
unless ($suffix=~/ome.tif/) {
  die "Input file $infile may not be an ome-tiff file (suffix doesn't match .ome.tif[f]).\n";
}

if (!$outfile) {
  die "ERROR: Output file name required.\n";
}

# Resolve path to tiffcomment utility
my $tiffcomment = qx/which tiffcomment/;
chomp($tiffcomment);
print STDERR "$tiffcomment\n";
if(!defined($tiffcomment) || $tiffcomment eq "") {
  $tiffcomment = dirname(abs_path($0))."/tiffcomment";
}
print STDERR "$tiffcomment\n";
unless(-x $tiffcomment) {
  die "ERROR: Can't find or run tiffcomment utility.\n";
}
# Extract XML from tiff header
print STDERR "Extracting XML from tiff header...\n" if $v;
my $xml = qx/$tiffcomment $infile/;
print STDERR "Done.\n" if $v;
# Remove debug text from tiffcomment output
if ($xml!~/^<\?xml version/) {
  $xml=~s/^[\S\s]+<\?xml version/<\?xml version/;
}
print STDERR $xml;
# Parse the whole XML
my $twig = XML::Twig->new();
$twig->parse($xml);
print $@ if $@;

# Find list of markers assumed to be in the same order as the channels
print STDERR "\nExtracting list of biomarkers used...\n" if $v;
my @markers;
my @metadata = $twig->findnodes("//XMLAnnotation/Value/OriginalMetadata");
foreach my $elt (@metadata) {
  if ($elt->first_child('Key')->text=~/Biomarker/) {
    my $markers = $elt->first_child('Value')->text;
    # Remove opening and closing brackets as well as extra whitespace
    $markers =~s/^\s*\[//;
    $markers =~s/\]\s*$//;
    # Extract marker names from comma-separated list
    @markers = split(/\s*,\s*/, $markers);
  }
}
my %seen;
$seen{$_}++ for @markers;
print STDERR "Done.\n" if $v;

# Find all occurrences of the Channel tag
print STDERR "\nReplacing current channel names with biomarker names...\n" if $v;
my @channels  = $twig->findnodes("//Channel");

if ($#markers != $#channels) {
  die "\nERROR: The number of markers isn't equal to the number of channels.\n";
}

my $counter = 0;
my %occurrences;
foreach ($twig->findnodes("//Channel")) {
  my $marker = $markers[$counter];
  if($seen{$marker}>1) {
    $occurrences{$marker}++;
    $marker = $marker."-".$occurrences{$marker};
  }
  $_->set_att( Name => $marker );
  $counter++;
}
print STDERR "Done.\n" if $v;

# Write the new XML
print STDERR "\nWriting new OME-XML into $outfile...\n" if $v;
$xml = $twig->sprint;
move($infile, $outfile) or die "Move failed: $!";
my $pid = open(FH, "| $tiffcomment -set - $outfile");
print FH $xml;
close(FH);


exit;


sub usage {
  print STDERR<<"END";

    This program edits the Channel tag from the XML header of an OME.TIFF file.
    It finds all occurrences of the tag and replaces the values of their Name
    attribute with biomarker names extracted from annotation metadata included
    in the XML (in StructuredAnnotation/XMLAnnotation elements).

    Usage: $0 [-help] -input file.ome.tiff [-output modified_file.ome.tiff]

     -h[elp]        : This message
     -i[nput]       : An OME.TIFF file.
     -o[utput]      : Name of the file to write the modified ome.tiff content to.

    example: $0 -i original/scan.ome.tiff -o modified/mod_scan.ome.tiff

END

  exit;
}
